

@RD /S /Q "./Releases"
@RD /S /Q "./Binaries"
@RD /S /Q "./Build"
@RD /S /Q "./DerivedDataCache"
@RD /S /Q "./Intermediate"
@RD /S /Q "./Saved"
@RD /S /Q "./Packaged"
@RD /S /Q "./.vs"


set PLUGIN_NAME=Plugins/MindhiveLSD
@RD /S /Q "./%PLUGIN_NAME%/Binaries"
@RD /S /Q "./%PLUGIN_NAME%/Intermediate"
@RD /S /Q "./%PLUGIN_NAME%/Saved"

set PLUGIN_NAME=Plugins/MindhiveShared
@RD /S /Q "./%PLUGIN_NAME%/Binaries"
@RD /S /Q "./%PLUGIN_NAME%/Intermediate"
@RD /S /Q "./%PLUGIN_NAME%/Saved"

